﻿namespace Sila2.Utils
{
    /// <summary>
    /// Provides methods to convert values between several units (to be extended on demand).
    /// </summary>
    public static class UnitConverter
    {
        #region Temperature

        public static double Kelvin2DegreeCelsius(double value) { return value - 273.15; }

        public static double DegreeCelsius2Kelvin(double value) { return value + 273.15; }

        public static double Kelvin2DegreeFahrenheit(double value) { return value * 9 / 5 - 459.67; }

        public static double DegreeFahrenheit2Kelvin(double value) { return (value + 459.67) * 5 / 9; }

        #endregion
    }
}