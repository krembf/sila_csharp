using System;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Sila2.Org.Silastandard;

namespace Sila2.Server.Command.Observable.Test
{
    using Sila2.Org.Silastandard.Examples.Testfeature.V1;

    public class TestFeatureImpl : TestFeature.TestFeatureBase
    {
        public Func<IProgress<ExecutionInfo>, TestObservable_Parameters, CancellationToken, TestObservable_Responses>
            TestObservableFunc;

        public readonly ObservableCommandManager<TestObservable_Parameters, TestObservable_Responses>
            ObservableCommandManager;

        public TestFeatureImpl(TimeSpan lifetimeOfExecution)
        {
            ObservableCommandManager
                = new ObservableCommandManager<TestObservable_Parameters, TestObservable_Responses>(lifetimeOfExecution);
        }

        public override async Task<CommandConfirmation> TestObservable(TestObservable_Parameters request,
            ServerCallContext context)
        {
            if (TestObservableFunc == null)
            {
                throw new NotImplementedException("The task for the command is not assigned");
            }

            var command = await ObservableCommandManager.AddCommand(request, TestObservableFunc);
            return command.Confirmation;
        }

        public override async Task TestObservable_Info(CommandExecutionUUID request,
            IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            await ObservableCommandManager.RegisterForInfo(request, responseStream, context.CancellationToken);
        }

        public override Task<TestObservable_Responses> TestObservable_Result(CommandExecutionUUID request,
            ServerCallContext context)
        {
            var command = ObservableCommandManager.GetCommand(request);
            return Task.FromResult(command.Result());
        }
    }
}