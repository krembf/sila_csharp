﻿using System.IO;
using Grpc.Core;

namespace Sila2.Utils
{
    /// <summary>
    /// Utility class for encryption support on gRPC
    /// </summary>
    public static class Encryption
    {
        #region Utils

        public static ServerCredentials CreateServerCredentials(string certificateFilePath, string keyFilePath)
        {
            var certificate = File.ReadAllText(certificateFilePath);
            var key = File.ReadAllText(keyFilePath);
            // root certificate must only be provided if we wish to authenticate the client
            return new SslServerCredentials(new[] {new KeyCertificatePair(certificate, key)});
        }

        /// <summary>
        /// Utility to create client SSL credentials from a specified CA 
        /// </summary>
        /// <param name="caFilePath">Path to root CA to use</param>
        /// <returns>gRPC SslCredentials created from root CA</returns>
        /// <exception cref="FileNotFoundException"></exception>
        public static SslCredentials CreateSslCredentials(string caFilePath)
        {
            if (!File.Exists(caFilePath)) throw new FileNotFoundException(caFilePath);
            var caCert = File.ReadAllText(caFilePath);
            return new SslCredentials(caCert);
        }

        #endregion
    }
}