namespace Sila2.Utils
{
    /// <summary>
    /// Interface used to implement access layers to a given server configuration
    /// </summary>
    public interface IConfigWrapper<TConfig>
    {
        /// <summary>
        /// Set the ServerConfig
        /// </summary>
        void SetConfig(TConfig config);

        /// <summary>
        /// Get the server configuration
        /// </summary>
        /// <returns>Server Configuration</returns>
        TConfig GetConfig();
    }
}