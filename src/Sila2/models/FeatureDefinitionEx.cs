﻿namespace Sila2
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// This class extends the Feature class auto-generated from the FeatureDefinition schema.
    /// </summary>
    public partial class Feature
    {
        /// <summary>
        /// Gets the namespace to be used when calling the gRPC service.
        /// </summary>
        public string Namespace => $"sila2.{this.Originator}.{this.Category}.{this.Identifier.ToLower()}.v{Version.Parse(this.FeatureVersion).Major}";

        #region Fully qualified identifiers

        /// <summary>
        /// Gets the fully qualified identifier assembled from several feature properties as specified in the SiLA 2 standard.
        /// </summary>
        public string FullyQualifiedIdentifier => $"{this.Originator}/{this.Category}/{this.Identifier}/v{Version.Parse(this.FeatureVersion).Major}";
        
        /// <summary>
        /// Returns the fully qualified identifier of the given command.
        /// </summary>
        /// <param name="commandIdentifier">The identifier of the command.</param>
        /// <returns>The fully qualified command identifier.</returns>
        public string GetFullyQualifiedCommandIdentifier(string commandIdentifier)
        {
            return $"{this.FullyQualifiedIdentifier}/Command/{commandIdentifier}";
        }

        /// <summary>
        /// Returns the fully qualified identifier of the given parameter of the given command.
        /// </summary>
        /// <param name="commandIdentifier">The identifier of the command.</param>
        /// <param name="parameterIdentifier">The identifier of the parameter.</param>
        /// <returns>The fully qualified command parameter identifier.</returns>
        public string GetFullyQualifiedCommandParameterIdentifier(string commandIdentifier, string parameterIdentifier)
        {
            return $"{this.GetFullyQualifiedCommandIdentifier(commandIdentifier)}/Parameter/{parameterIdentifier}";
        }

        /// <summary>
        /// Returns the fully qualified identifier of the given response of the given command.
        /// </summary>
        /// <param name="commandIdentifier">The identifier of the command.</param>
        /// <param name="responseIdentifier">The identifier of the response.</param>
        /// <returns>The fully qualified command response identifier.</returns>
        public string GetFullyQualifiedCommandResponseIdentifier(string commandIdentifier, string responseIdentifier)
        {
            return $"{this.GetFullyQualifiedCommandIdentifier(commandIdentifier)}/Response/{responseIdentifier}";
        }
        
        /// <summary>
        /// Returns the fully qualified identifier of the given metadata.
        /// </summary>
        /// <param name="commandIdentifier">The identifier of the command.</param>
        /// <param name="intermediateResponseIdentifier">The identifier of the intermediate response.</param>
        /// <returns>The fully qualified intermediate command response identifier.</returns>
        public string GetFullyQualifiedIntermediateCommandResponseIdentifier(string commandIdentifier, string intermediateResponseIdentifier)
        {
            return $"{this.GetFullyQualifiedCommandIdentifier(commandIdentifier)}/IntermediateResponse/{intermediateResponseIdentifier}";
        }
        /// <summary>
        /// Returns the fully qualified identifier of the given defined execution error.
        /// </summary>
        /// <param name="definedExecutionErrorIdentifier">The identifier of the defined execution error.</param>
        /// <returns>The fully qualified defined execution error identifier.</returns>
        public string GetFullyQualifiedDefinedExecutionErrorIdentifier(string definedExecutionErrorIdentifier)
        {
            return $"{this.FullyQualifiedIdentifier}/DefinedExecutionError/{definedExecutionErrorIdentifier}";
        }

        /// <summary>
        /// Returns the fully qualified identifier of the given property.
        /// </summary>
        /// <param name="propertyIdentifier">The identifier of the property.</param>
        /// <returns>The fully qualified property identifier.</returns>
        public string GetFullyQualifiedPropertyIdentifier(string propertyIdentifier)
        {
            return $"{this.FullyQualifiedIdentifier}/Property/{propertyIdentifier}";
        }

        /// <summary>
        /// Returns the fully qualified identifier of the given data type.
        /// </summary>
        /// <param name="dataTypeIdentifier">The identifier of the data type.</param>
        /// <returns>The fully qualified data type identifier.</returns>
        public string GetFullyQualifiedDataTypeIdentifier(string dataTypeIdentifier)
        {
            return $"{this.FullyQualifiedIdentifier}/DataType/{dataTypeIdentifier}";
        }

        /// <summary>
        /// Returns the fully qualified identifier of the given metadata.
        /// </summary>
        /// <param name="metadataIdentifier">The identifier of the metadata.</param>
        /// <returns>The fully qualified metadata identifier.</returns>
        public string GetFullyQualifiedMetadataIdentifier(string metadataIdentifier)
        {
            return $"{this.FullyQualifiedIdentifier}/Metadata/{metadataIdentifier}";
        }

        #endregion

        /// <summary>
        /// Returns all DefinedExecutionErrors that are defined in the Feature definition.
        /// </summary>
        /// <returns>A list of all DefinedExecutionErrors.</returns>
        public List<FeatureDefinedExecutionError> GetDefinedExecutionErrors()
        {
            var result = new List<FeatureDefinedExecutionError>();

            foreach (var item in Items)
            {
                if (item is FeatureDefinedExecutionError error)
                {
                    result.Add(error);
                }
            }

            return result;
        }

        /// <summary>
        /// Returns all commands that are defined in the Feature definition.
        /// </summary>
        /// <returns>A list of all commands.</returns>
        public List<FeatureCommand> GetDefinedCommands()
        {
            var result = new List<FeatureCommand>();

            foreach (var item in this.Items)
            {
                if (item is FeatureCommand command)
                {
                    result.Add(command);
                }
            }

            return result;
        }

        #region Overrides of Object

        /// <summary>
        /// Lists the Featrue Definition content in a readable multiline manner.
        /// </summary>
        /// <returns>The feature definition multiline text string.</returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            foreach (var property in this.GetType().GetProperties())
            {
                switch (property.Name)
                {
                    case "Items":
                        break;

                    default:
                        sb.Append(property.Name);

                        sb.Append(": ");
                        if (property.GetIndexParameters().Length > 0)
                        {
                            sb.Append("Indexed Property cannot be used");
                        }
                        else
                        {
                            sb.Append(property.GetValue(this, null));
                        }

                        sb.Append(Environment.NewLine);
                        break;
                }
            }

            // items
            sb.Append(Environment.NewLine);
            foreach (var item in this.Items)
            {
                if (item is FeatureCommand command)
                {
                    sb.AppendFormat("<Command> \"{0}\": {1}\n", command.Identifier, command.Description);
                    if (command.Parameter != null)
                    {
                        sb.AppendLine("   Parameters:");
                        foreach (var parameter in command.Parameter)
                        {
                            sb.AppendFormat("   - \"{0}\" [{1}]: {2}\n", parameter.Identifier, GetDataTypeString(parameter.DataType), parameter.Description);
                        }
                    }

                    if (command.Response != null)
                    {
                        sb.AppendLine("   Responses:");
                        foreach (var response in command.Response)
                        {
                            sb.AppendFormat("   - \"{0}\" [{1}]: {2}\n", response.Identifier, GetDataTypeString(response.DataType), response.Description);
                        }
                    }

                    if (command.IntermediateResponse != null)
                    {
                        sb.AppendLine("   Intermediate Responses:");
                        foreach (var intermediateResponse in command.IntermediateResponse)
                        {
                            sb.AppendFormat("   - \"{0}\" [{1}]: {2}\n", intermediateResponse.Identifier, GetDataTypeString(intermediateResponse.DataType), intermediateResponse.Description);
                        }
                    }

                    if (command.DefinedExecutionErrors != null)
                    {
                        sb.AppendLine("   Defined Execution Errors:");
                        foreach (var error in command.DefinedExecutionErrors)
                        {
                            sb.AppendFormat("   - \"{0}\"\n", error);
                        }
                    }

                }
                else if (item is FeatureDefinedExecutionError error)
                {
                    sb.AppendFormat("<DefinedExecutionError> \"{0}\": {1}\n", error.Identifier, error.Description);
                }
                else if (item is FeatureProperty property)
                {
                    sb.AppendFormat("<Property> \"{0}\" {1} [{2}]: {3}\n", property.Identifier, property.Observable, GetDataTypeString(property.DataType), property.Description);
                }
                else if (item is FeatureMetadata metaData)
                {
                    sb.AppendFormat("<MetaData> \"{0}\" [{1}]: {2}\n", metaData.Identifier, GetDataTypeString(metaData.DataType), metaData.Description);
                }
                else if (item is SiLAElement dataType)
                {
                    sb.AppendFormat("<DataTypeDefinition> \"{0}\" [{1}]: {2}\n", dataType.Identifier, GetDataTypeString(dataType.DataType), dataType.Description);
                }
            }

            return sb.ToString();
        }

        private static string GetDataTypeString(DataTypeType type)
        {
            if (type.Item is BasicType)
            {
                // basic type
                return type.Item.ToString();
            }

            if (type.Item is ConstrainedType constrainedType)
            {
                // constrained type
                return $"constrained {GetDataTypeString(constrainedType.DataType)}";
            }

            if (type.Item is StructureType structureType)
            {
                // structure type
                return $"structured {structureType.Element}";
            }

            if (type.Item is ListType listType)
            {
                // list type
                return $"list of {GetDataTypeString(listType.DataType)}";
            }

            // custom type
            return type.Item.ToString();
        }

        #endregion
    }
}