namespace Sila2
{
    using System.Collections.Generic;
    using Grpc.Core;

    public class ServerData
    {
        public Channel Channel { get; }
        public ServerConfig Config { get; }
        public ServerInformation Info { get; }
        public List<Feature> Features { get; }

        /// <summary>
        /// Server model constructor (immutable object)
        /// </summary>
        /// <param name="channel">gRPC channel to connect to server</param>
        /// <param name="serverConfig">Server configuration data</param>
        /// <param name="serverInformation">Server information</param>
        /// <param name="features">Server features</param>
        public ServerData(Channel channel, ServerConfig serverConfig, ServerInformation serverInformation, List<Feature> features)
        {
            Config = serverConfig;
            Info = serverInformation;
            Features = features;
            Channel = channel;
        }

        /// <summary>
        /// Overriden method
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            // read properties from SiLAService feature
            var str = $"{Config}\n{Info}\ngRPC Channel: {Channel.Target}";
            // feature discovery
            str += $"Feature Discovery:\n";
            // get implemented features
            str += $"{Features.Count} implemented Feature{(Features.Count == 1 ? string.Empty : "s")} found:";
            foreach (var f in Features)
            {
                str += $"\n-------------------------------------------------------------------------\n";
                str += $" Feature Definition of {f.FullyQualifiedIdentifier}\n";
                str += $"---------------------------------------------------------------------------\n";
                str += $"{f}\n";
            }
            return str;
        }
    }
}