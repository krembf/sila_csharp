namespace Sila2
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// Server configuration data containing server properties which may change 
    /// at runtime or are configurable.
    /// 
    /// TODO: members could be private with?
    /// </summary>
    [DataContract]
    public class ServerConfig
    {
        /// <summary>
        /// Configurable name of the server
        /// </summary>
        [DataMember(IsRequired = true)]
        public string Name;
        /// <summary>
        /// Guid of the server, loaded from config or generated
        /// </summary>
        [DataMember(IsRequired = true)]
        public readonly Guid Uuid;
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="uuid"></param>
        public ServerConfig(string name, Guid uuid)
        {
            Name = name;
            Uuid = uuid;
        }

        public override string ToString()
        {
            return $"Server Name: {Name}" + 
            $"\nServer Id: {Uuid}\n";
        }
    }
}