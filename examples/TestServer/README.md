# DataTypeProvider
A simple sila server which provides a feature with a complex set of 
input and output data structures.


## Generate the Feature Stubs
In order to generate the feature stubs

```
cd TestServer/TestServer
generate_stubs.cmd
```