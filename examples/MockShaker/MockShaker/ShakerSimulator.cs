using System;
using System.Threading.Tasks;

namespace Sila2.Examples.MockShaker
{
    using Common.Logging;

    public enum ClampState
    {
        Closed,
        Open
    }

    [Serializable]
    public class ClampNotClosedException : Exception
    {
        public ClampNotClosedException(string message)
            : base(message)
        {
        }
    }

    public class ShakerSimulator
    {
        private static readonly ILog _log = LogManager.GetLogger<ShakerSimulator>();
        private bool shaking;
        private ClampState clampState;

        public const int MaxShakingDuration = 5; //[mins]
        public double Progress;
        public TimeSpan RemainingTime;

        public bool IsShaking
        {
            get => shaking;
        }

        public ClampState ClampState
        {
            get => clampState;
            set
            {
                _log.Info($"ClampState: {value}");
                clampState = value;
            }
        }

        public void Stop()
        {
            shaking = false;
        }

        /// <summary>
        /// Simulates shake operation for a specified time period
        /// </summary>
        /// <param name="duration">max shaking duration of 1min</param>
        /// <returns></returns>
        public async Task Shake(TimeSpan duration)
        {
            if (duration > TimeSpan.FromMinutes(MaxShakingDuration))
            {
                throw new Exception($"Specified duration {duration} exceeds max duration ");
            }
            // check if clamp is closed
            if (ClampState != ClampState.Closed)
            {
                throw new ClampNotClosedException("Unable to shake with opened clamp");
            }

            _log.Info("Shake it..");
            shaking = true;
            var startTime = DateTime.Now;
            var endTime = startTime + duration;
            var currentTime = startTime;
            while (currentTime < endTime && shaking)
            {
                _log.Info("Shake shake it!");
                Progress = (currentTime - startTime).Seconds / duration.Seconds;
                RemainingTime = endTime - currentTime;
                await Task.Delay(300);
                _log.Info("Shake it..");
                await Task.Delay(300);
                currentTime = DateTime.Now;
            }

            _log.Info("Shaking it like a polaroid picture!");
        }
    }
}