﻿namespace Sila2.Examples.MockShaker.ClientApp
{
    using System;
    using System.Net;
    using Grpc.Core;
    using System.Linq;
    using Utils;
    using Discovery;
    using Common.Logging;
    using CommandLine;

    class Program
    {
        private static void DisplayClampState(Client client)
        {
            Console.WriteLine("Clamp state = " + (client.IsClampOpen() ? "open" : "closed"));
        }

        static void Main(string[] args)
        {
            Logging.SetupCommonLogging();
            var log = LogManager.GetLogger<Program>();

            try
            {
                Channel channel = null;
                ClientCmdLineArgs options = null;
                Parser.Default.ParseArguments<ClientCmdLineArgs>(args)
                    .WithParsed<ClientCmdLineArgs>(o => options = o)
                    .WithNotParsed(ClientCmdLineArgs.HandleParseError);
                if (options.IpAddress == null && options.Port == -1)
                {
                    const string namePattern = "Mock Shaker";
                    var server = SiLADiscovery.GetServers(3000).FirstOrDefault(s => s.Config.Name == namePattern);
                    if (server == null)
                    {
                        throw new InvalidOperationException($"No server named: {namePattern} found");
                    }
                    channel = server.Channel;
                }
                else
                {
                    channel = ServiceFinder.BuildNewChannel(IPAddress.Parse(options.IpAddress), options.Port);
                }
                // create the client
                var client = new Client(channel);

                // test clamp handling
                DisplayClampState(client);
                client.OpenClamp();
                DisplayClampState(client);

                // try shaking with invalid duration parameter
                try
                {
                    client.Shake("7S", 3000).Wait();
                }
                catch (Exception e)
                {
                    Console.WriteLine(ErrorHandling.HandleException(e));
                }

                // try shaking with opened clamp
                try
                {
                    client.Shake("PT7S", 3000).Wait();
                }
                catch (Exception e)
                {
                    Console.WriteLine(ErrorHandling.HandleException(e));
                }

                // close clamp in order to shake
                client.CloseClamp();
                DisplayClampState(client);

                // shake for 7 seconds
                client.Shake("PT7S", 3000).Wait();

                Console.WriteLine("Shutting down connection...");

                channel.ShutdownAsync().Wait();
                Console.WriteLine("Press any key to exit...");
            }
            catch (Exception e)
            {
                log.Error(e);
            }

            Console.ReadKey();
        }
    }
}