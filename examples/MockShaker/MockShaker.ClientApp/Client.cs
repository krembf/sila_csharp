﻿namespace Sila2.Examples.MockShaker.ClientApp
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.IO;
    using System.Runtime.Serialization.Formatters.Binary;
    using Common.Logging;
    using Grpc.Core;

    using SiLAFramework = Org.Silastandard;
    using Shaking = De.Equicon.Mixing.Shakingcontrol.V1;
    using Handling = De.Equicon.Handling.Platehandlingcontrol.V1;
    using Utils;

    public class Client
    {
        private readonly Shaking.ShakingControl.ShakingControlClient _shakingClient;
        private readonly Handling.PlateHandlingControl.PlateHandlingControlClient _handlingClient;
        private static ILog Log = LogManager.GetLogger<Client>();

        public Client(Channel channel)
        {

            this._shakingClient = new Shaking.ShakingControl.ShakingControlClient(channel);
            this._handlingClient = new Handling.PlateHandlingControl.PlateHandlingControlClient(channel);
        }

        #region ShakingControl commands

        public async Task Shake(string duration, int speed)
        {
            var cmdId = _shakingClient.Shake(new Shaking.Shake_Parameters { Duration = new SiLAFramework.String { Value = duration }, Speed = new SiLAFramework.Integer { Value = speed } }).CommandExecutionUUID;

            Log.Info($"Shake command started with speed: {speed} and duration: {duration} ...");

            // wait for progress has been finished
            try
            {
                using (var call = _shakingClient.Shake_Info(cmdId))
                {
                    var responseStream = call.ResponseStream;

                    while (await responseStream.MoveNext())
                    {
                        Console.Write("\rProgress: {0:0.00}     Status: {1}", responseStream.Current.ProgressInfo.Value, responseStream.Current.CommandStatus);
                        if (responseStream.Current.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedSuccessfully || responseStream.Current.CommandStatus == SiLAFramework.ExecutionInfo.Types.CommandStatus.FinishedWithError)
                        {
                            break;
                        }
                    }
                    Console.Write("\n");
                }
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                throw;
            }

            // get result
            try
            {
                var response = _shakingClient.Shake_Result(cmdId);
                Log.Info("Shake command response received: " + response);
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                throw;
            }
        }

        #endregion

        #region Plate Handling commands

        public void OpenClamp()
        {
            Log.Info("Opening clamp");
            Handling.OpenClamp_Responses response;
            try
            {
                response = _handlingClient.OpenClamp(new Handling.OpenClamp_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                throw;
            }
        }

        public void CloseClamp()
        {
            Log.Info("Closing clamp...");
            Handling.CloseClamp_Responses response;
            try
            {
                response = _handlingClient.CloseClamp(new Handling.CloseClamp_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                throw;
            }
        }

        public bool IsClampOpen()
        {
            Handling.Get_CurrentClampState_Responses response;
            try
            {
                response = _handlingClient.Get_CurrentClampState(new Handling.Get_CurrentClampState_Parameters());
            }
            catch (RpcException e)
            {
                Log.Error(ErrorHandling.HandleException(e));
                throw;
            }

            return response.CurrentClampState.Value;
        }

        #endregion

        #region Helper methods

        public static object StringToObject(byte[] bytes)
        {
            using (MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length))
            {
                ms.Write(bytes, 0, bytes.Length);
                ms.Position = 0;
                return new BinaryFormatter().Deserialize(ms);
            }
        }
        #endregion

        public void StopShaking()
        {
            _shakingClient.StopShaking(new Shaking.StopShaking_Parameters());
        }
    }
}