using System;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;
using Sila2.Org.Silastandard.Examples.Temperaturecontroller.V1;
using Sila2.Utils;

namespace Sila2.Examples.HelloSila
{
    public class ThermostatSimulator
    {
        private static readonly ILog _log = LogManager.GetLogger<ThermostatSimulator>();
        public const double KELVIN_PER_SECONDS = 4; // [K/s] Update ramp
        private const double KELVIN_ACCURACY = 1; // [K] When control is assumed to be finished
        private const double UPDATE_INTERVAL = 1; // [s] Interval temperature is updated
        private const double KELVIN_RAMP = KELVIN_PER_SECONDS * UPDATE_INTERVAL; // [Kelvin / step]
        private readonly object _lock = new object();
        public double CurrentTemperature
        {
            get => GetCurrentTemperature();
        }
        public double TargetTemperature { get; private set; }
        private double _currentTemperature = UnitConverter.DegreeCelsius2Kelvin(20);

        public void SetTargetTemperature(double temperature, CancellationToken cancellationToken)
        {
            TargetTemperature = temperature;
            _log.Info($"Target Temperature: {TargetTemperature}");
            _log.Info($"Current Temperature: {_currentTemperature}");
            var temperatureSet = false;
            while (!(temperatureSet || cancellationToken.IsCancellationRequested))
            {
                var startTime = DateTime.Now;
                // Linear Increase to target temperature until accuracy reached
                var temperatureDifference = TargetTemperature - _currentTemperature;
                var absoluteTemperatureDifference = Math.Abs(temperatureDifference);

                if (absoluteTemperatureDifference >= KELVIN_ACCURACY) {
                    if (KELVIN_RAMP > absoluteTemperatureDifference)
                    {
                        _currentTemperature = TargetTemperature;
                    } else {
                        lock (_lock)
                        {
                            _currentTemperature += Math.Sign(temperatureDifference) * KELVIN_RAMP;
                        }
                    }
                }
                else
                {
                    temperatureSet = true;
                }
                // Best effort to keep the update interval
                var remainingTime =  TimeSpan.FromSeconds(UPDATE_INTERVAL) - (DateTime.Now - startTime);
                if (remainingTime > TimeSpan.Zero)
                    Thread.Sleep((int) remainingTime.TotalMilliseconds);
            }

            _log.Info($"End of temperature change agent");
            _log.Info($"Current Temperature: {_currentTemperature}");
        }

        private double GetCurrentTemperature()
        {
            lock (_lock)
            {
                return _currentTemperature;
            }
        }
    }
    
}