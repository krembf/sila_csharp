using System.Threading.Tasks;
using Sila2.Ch.Unitelabs.Robot.Platecalibrationservice.V1;
using Sila2.Ch.Unitelabs.Robot.Robotcontroller.V1;

namespace RobotControl
{
    public enum Orientation
    {
        Landscape,
        Portrait
    }
    /// <summary>
    /// Interface which represents interactions with the robot. 
    ///
    /// For example, if there is a REST API to command the robot to power on, acquire battery status or perform
    /// movement operations then the derived class of this interface would be responsible for such robot interactions.
    /// 
    /// </summary>
    public interface IRobotController
    {
        double BatteryPercentage { get; }
        int BatteryEstimatedTime { get; }
        /// <summary>
        /// Mode of the robot in 'free drive', to be able to controlled by human
        /// </summary>
        bool TeachMode { get; set; }

        /// <summary>
        /// Initialize and home the robot before operations can be done
        /// </summary>
        Task Initialize();
        
        Task MoveToPosition(string positionName);

        /// <summary>
        /// Move to device at specified index
        /// </summary>
        /// <param name="device">name of the device</param>
        /// <param name="siteIndex">index of the site in the device to move to</param>
        Task MoveToSite(DataType_Site.Types.Site_Struct site);

        Task Retract();

        /// <summary>
        /// Move the robot and approach to the device at specified site
        ///
        /// <remarks>
        /// Mobile Robot: would move to the position to reach the device,
        /// and orientation but arm would NOT be moved in any of this operation
        /// </remarks>
        ///
        /// <remarks>
        /// Static Robot: would move the arm to the approach height of the target device
        /// </remarks>
        /// </summary>
        /// <param name="device">name of the device</param>
        /// <param name="siteIndex">index of the site in the device to move to</param>
        Task ApproachTo(DataType_Site.Types.Site_Struct site);

        /// <summary>
        /// Move labware from source site at device origin to destination site at destination device
        /// </summary>
        /// <param name="srcDevice">name of origin device</param>
        /// <param name="srcSiteIndex">index of site</param>
        /// <param name="dstDevice">name of destination device</param>
        /// <param name="dstSiteIndex">index of site</param>
        Task MovePlate(DataType_Site.Types.Site_Struct srcSite, DataType_Site.Types.Site_Struct dstSite, string plateType);
        Task PickPlate(DataType_Site.Types.Site_Struct site, string plateType);
        Task PlacePlate(DataType_Site.Types.Site_Struct site, string plateType);
        bool IsOccupied(DataType_Site.Types.Site_Struct site);

        /// <summary>
        /// Moves the robot to the charging station, and charges
        /// </summary>
        Task Charge();

        /// <summary>
        /// Opens or closes a gripper
        /// </summary>
        /// <param name="close">grip closed if true, releases if false</param>
        /// <returns></returns>
        Task Grip(bool close);

        /// <summary>
        /// Cancels any currently running tasks
        /// </summary>
        /// <returns></returns>
        Task Cancel();

        Task GripCalibrationPlate(Orientation plateOrientation);
    }
}