namespace RobotControl
{
    public class Device
    {
        public string Name { get; set; }
        public int NumberOfSites { get; set; }
        public int AccessHeight { get; set; }
        public int ApproachDistance { get; set; }
    }
}