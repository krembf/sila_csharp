using System;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;

namespace RobotControl.Impl
{
    public class SynchronizationController
    {
        private static readonly SemaphoreSlim _robotSemaphore = new SemaphoreSlim(1, 1);
        private static readonly ILog _log = LogManager.GetLogger<SynchronizationController>();
        public static Task Execute(Action method)
        {
            if (_robotSemaphore.CurrentCount == 0)
            {
                throw new Exception("Failed to execute command, other operation undergoing");
            }
            _log.Debug($"Attempting to call {method.Method}");
            try
            {
                _robotSemaphore.Wait();
                _log.Debug($"calling {method.Method}");
                method.Invoke();
            }
            finally
            {
                _robotSemaphore.Release();   
            }
            return Task.CompletedTask;
        }
    }
}