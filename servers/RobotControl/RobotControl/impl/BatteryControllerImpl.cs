using System;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Sila2;
using Sila2.Org.Silastandard;
using Sila2.Utils;

namespace RobotControl.Impl
{
    using Sila2.Ch.Unitelabs.Robot.Batterycontroller.V1;

    public class BatteryControllerImpl : BatteryController.BatteryControllerBase
    {
        private static readonly ILog _log = LogManager.GetLogger<RobotControllerImpl>();
        private readonly IRobotController _robotController;

        private readonly ObservableCommandManager<Charge_Parameters, Charge_Responses> _chargeCommandManager =
            new ObservableCommandManager<Charge_Parameters, Charge_Responses>(TimeSpan.FromDays(1));

        public BatteryControllerImpl(IRobotController robotController)
        {
            _robotController = robotController;
        }
        
        public override async Task<CommandConfirmation> Charge(Charge_Parameters request, ServerCallContext context)
        {
            var command = await _chargeCommandManager.AddCommand(request, ChargeTask());
            return command.Confirmation;
        }

        public override async Task Charge_Info(CommandExecutionUUID request,
            IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _chargeCommandManager.RegisterForInfo(request, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                _log.Error(e);
                throw;
            }
        }

        public override Task<Charge_Responses> Charge_Result(CommandExecutionUUID request, ServerCallContext context)
        {
            var command = _chargeCommandManager.GetCommand(request);
            return Task.FromResult(command.Result());
        }

        public override async Task Subscribe_BatteryPercentage(Subscribe_BatteryPercentage_Parameters request, IServerStreamWriter<Subscribe_BatteryPercentage_Responses> responseStream,
            ServerCallContext context)
        {
            var response = new Subscribe_BatteryPercentage_Responses();
            while (!context.CancellationToken.IsCancellationRequested)
            {
                await Task.Delay(3000);
                SynchronizationController.Execute(() =>
                {
                    response.BatteryPercentage = new Real {Value = _robotController.BatteryPercentage};
                });
                await responseStream.WriteAsync(response);
            }
        }

        public override async Task Subscribe_BatteryRemainingTime(Subscribe_BatteryRemainingTime_Parameters request,
            IServerStreamWriter<Subscribe_BatteryRemainingTime_Responses> responseStream, ServerCallContext context)
        {
            var response = new Subscribe_BatteryRemainingTime_Responses();
            while (!context.CancellationToken.IsCancellationRequested)
            {
                await Task.Delay(3000);
                await SynchronizationController.Execute(() =>
                {
                    response.BatteryRemainingTime = new Real {Value = _robotController.BatteryEstimatedTime};
                });
                await responseStream.WriteAsync(response);
            }
        }
        
        private Func<IProgress<ExecutionInfo>, Charge_Parameters, CancellationToken, Charge_Responses> ChargeTask()
        {
            return (progress, parameters, cancellationToken) =>
            {
                try
                {
                    SynchronizationController.Execute(() =>
                    {
                        //TODO remove the wait and make Async
                        _robotController.Charge().Wait(cancellationToken);
                    }).Wait(cancellationToken);
                }
                catch (Exception e)
                {
                    _log.Warn(e);
                    var error = ErrorHandling.CreateUndefinedExecutionError(e.Message);
                    //TODO: Narrow down and handle "InaccessibleSite" and "SiteNotFound
                    ErrorHandling.RaiseSiLAError(error);
                }

                return new Charge_Responses();
            };
        }

    }
}