using System;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using Sila2;
using Sila2.Ch.Unitelabs.Robot.Robotcontroller.V1;
using Sila2.Org.Silastandard;
using Sila2.Utils;

namespace RobotControl.Impl
{
    using SiLAFramework = Sila2.Org.Silastandard;

    public class RobotControllerImpl : RobotController.RobotControllerBase
    {
        private static readonly ILog _log = LogManager.GetLogger<RobotControllerImpl>();
        private readonly IRobotController _robotController;
        
        private readonly ObservableCommandManager<MoveToSite_Parameters, MoveToSite_Responses> _moveToSiteCommandManager =
            new ObservableCommandManager<MoveToSite_Parameters, MoveToSite_Responses>(TimeSpan.FromDays(1));
        private readonly ObservableCommandManager<MoveToPosition_Parameters, MoveToPosition_Responses>
            _moveToPositionCommandManager =
                new ObservableCommandManager<MoveToPosition_Parameters, MoveToPosition_Responses>(TimeSpan.FromDays(1));
        private readonly ObservableCommandManager<Retract_Parameters, Retract_Responses> _retractCommandManager =
            new ObservableCommandManager<Retract_Parameters, Retract_Responses>(TimeSpan.FromDays(1));
        private readonly ObservableCommandManager<ApproachTo_Parameters, ApproachTo_Responses> _approachToCommandManager =
            new ObservableCommandManager<ApproachTo_Parameters, ApproachTo_Responses>(TimeSpan.FromDays(1));
        private readonly ObservableCommandManager<MovePlate_Parameters, MovePlate_Responses> _movePlateCommandManager =
            new ObservableCommandManager<MovePlate_Parameters, MovePlate_Responses>(TimeSpan.FromDays(1));
        private readonly ObservableCommandManager<CheckOccupied_Parameters, CheckOccupied_Responses> _checkOccupiedCommandManager =
            new ObservableCommandManager<CheckOccupied_Parameters, CheckOccupied_Responses>(TimeSpan.FromDays(1));

        public RobotControllerImpl(IRobotController robotController)
        {
            _robotController = robotController;
        }
        
        public override async Task<CommandConfirmation> Retract(Retract_Parameters request, ServerCallContext context)
        {
            var command = await _retractCommandManager.AddCommand(request, RetractCommandTask);
            return command.Confirmation;
        }

        public override async Task Retract_Info(CommandExecutionUUID request, IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _retractCommandManager.RegisterForInfo(request, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                _log.Error(e);
                throw;
            }
        }

        public override Task<Retract_Responses> Retract_Result(CommandExecutionUUID request, ServerCallContext context)
        {
            var command = _retractCommandManager.GetCommand(request);
            return Task.FromResult(command.Result());
        }

        public override Task<PickPlate_Responses> PickPlate(PickPlate_Parameters request, ServerCallContext context)
        {
            Execute(() => _robotController.PickPlate(request.Site.Site, request.PlateType.Value).Wait());
            return Task.FromResult<PickPlate_Responses>(new PickPlate_Responses());
        }

        public override Task<PlacePlate_Responses> PlacePlate(PlacePlate_Parameters request, ServerCallContext context)
        {
            Execute(() => _robotController.PlacePlate(request.Site.Site, request.PlateType.Value).Wait());
            return Task.FromResult<PlacePlate_Responses>(new PlacePlate_Responses());
        }

        public override async Task<CheckOccupied_Responses> CheckOccupied(CheckOccupied_Parameters request, ServerCallContext context)
        {
            var response = new CheckOccupied_Responses { Occupied = new SiLAFramework.Boolean()};
            Execute(() => response.Occupied.Value = _robotController.IsOccupied(request.Site.Site));
            return response;
        }

        public override async Task<CommandConfirmation> MoveToPosition(MoveToPosition_Parameters request,
            ServerCallContext context)
        {
            var command = await _moveToPositionCommandManager.AddCommand(request, MoveToPositionTask());
            return command.Confirmation;
        }

        public override async Task MoveToPosition_Info(CommandExecutionUUID request,
            IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _moveToPositionCommandManager.RegisterForInfo(request, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                _log.Error(e);
                throw;
            }
        }

        public override Task<MoveToPosition_Responses> MoveToPosition_Result(CommandExecutionUUID request,
            ServerCallContext context)
        {
            var command = _moveToPositionCommandManager.GetCommand(request);
            return Task.FromResult(command.Result());
        }

        public override async Task<CommandConfirmation> MoveToSite(MoveToSite_Parameters request, ServerCallContext context)
        {
            var command = await _moveToSiteCommandManager.AddCommand(request, MoveToSiteTask());
            return command.Confirmation;
        }

        public override async Task MoveToSite_Info(CommandExecutionUUID request,
            IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _moveToSiteCommandManager.RegisterForInfo(request, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                _log.Error(e);
                throw;
            }
        }

        public override Task<MoveToSite_Responses> MoveToSite_Result(CommandExecutionUUID request, ServerCallContext context)
        {
            var command = _moveToSiteCommandManager.GetCommand(request);
            return Task.FromResult(command.Result());
        }

        public override async Task<CommandConfirmation> ApproachTo(ApproachTo_Parameters request, ServerCallContext context)
        {
            var command = await _approachToCommandManager.AddCommand(request, ApproachToTask());
            return command.Confirmation;
        }

        public override async Task ApproachTo_Info(CommandExecutionUUID request,
            IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _approachToCommandManager.RegisterForInfo(request, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                _log.Error(e);
                throw;
            }
        }

        public override Task<ApproachTo_Responses> ApproachTo_Result(CommandExecutionUUID request,
            ServerCallContext context)
        {
            var command = _approachToCommandManager.GetCommand(request);
            return Task.FromResult(command.Result());
        }

        public override async Task<CommandConfirmation> MovePlate(MovePlate_Parameters request, ServerCallContext context)
        {
            var command = await _movePlateCommandManager.AddCommand(request, MovePlateTask());
            return command.Confirmation;
        }

        public override async Task MovePlate_Info(CommandExecutionUUID request,
            IServerStreamWriter<ExecutionInfo> responseStream, ServerCallContext context)
        {
            try
            {
                await _movePlateCommandManager.RegisterForInfo(request, responseStream, context.CancellationToken);
            }
            catch (Exception e)
            {
                _log.Error(e);
                throw;
            }
        }

        public override Task<MovePlate_Responses> MovePlate_Result(CommandExecutionUUID request,
            ServerCallContext context)
        {
            var command = _movePlateCommandManager.GetCommand(request);
            return Task.FromResult(command.Result());
        }

        /// <summary>
        /// Move to operation wrapped in a task 
        /// </summary>
        /// <returns>Returns the obeservable command result</returns>
        private Func<IProgress<ExecutionInfo>, MoveToSite_Parameters, CancellationToken, MoveToSite_Responses> MoveToSiteTask()
        {
            return (progress, parameters, arg3) => 
            {
                Execute(() =>
                {
                    //TODO remove the wait and make Async
                    _robotController.MoveToSite(parameters.Site.Site).Wait(); 
                });
                return new MoveToSite_Responses();
            };
        }

        /// <summary>
        /// Creates move task to pass to observable command manager
        /// </summary>
        /// <returns>Anonymous function which wraps the execution of the Movement task</returns>
        private Func<IProgress<ExecutionInfo>, MoveToPosition_Parameters, CancellationToken, MoveToPosition_Responses>
        MoveToPositionTask()
        {
            return (progress, parameters, cancellationToken) => 
            {
                var deviceName = parameters.Position.Value;
                Execute(() =>
                {
                    //TODO remove the wait and make Async
                    _robotController.MoveToPosition(deviceName).Wait(cancellationToken);
                });

                return new MoveToPosition_Responses();
            };
        }


        /// <summary>
        /// Create anonymous fundtion of Approach To operation
        /// </summary>
        /// <returns>Returns the obeservable command result</returns>
        private Func<IProgress<ExecutionInfo>, ApproachTo_Parameters, CancellationToken, ApproachTo_Responses> 
        ApproachToTask()
        {
            
            return (progress, parameters, arg3) => 
            {
                Execute(() =>
                {
                    //TODO remove the wait and make Async
                    _robotController.ApproachTo(parameters.Site.Site).Wait(arg3);
                });
                return new ApproachTo_Responses();
            };
        }

        /// <summary>
        /// Move Plate operation wrapped in a task 
        /// </summary>
        /// <returns>Returns the obeservable command result</returns>
        private Func<IProgress<ExecutionInfo>, MovePlate_Parameters, CancellationToken, MovePlate_Responses>
            MovePlateTask()
        {
            return (progress, parameters, token) => 
            {
                Execute(() =>
                {
                    _robotController.MovePlate(parameters.OriginSite.Site, parameters.DestinationSite.Site, parameters.PlateType.Value).Wait();
                    //TODO remove the wait and make Async
                });
             
                return new MovePlate_Responses();
            };
        }

        private Retract_Responses RetractCommandTask(IProgress<ExecutionInfo> progress, Retract_Parameters parameters, CancellationToken cancellationToken)
        {
            Execute(() => _robotController.Retract().Wait(cancellationToken));
            return new Retract_Responses();
        }


        private static Task Execute(Action action)
        {
            try
            {
                return SynchronizationController.Execute(action.Invoke);
            }
            catch (Exception e)
            {
                _log.Warn(e);
                var error = ErrorHandling.CreateUndefinedExecutionError(e.Message);
                //TODO: Narrow down and handle "InaccessibleSite" and "SiteNotFound
                ErrorHandling.RaiseSiLAError(error);
            }
            return Task.CompletedTask;
        }
    }
}