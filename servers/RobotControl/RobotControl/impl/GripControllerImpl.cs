using System;
using System.Threading.Tasks;
using Common.Logging;
using Grpc.Core;
using RobotControl;
using Sila2.Ch.Unitelabs.Robot.Gripcontroller.V1;
using Sila2.Utils;

namespace RobotControl.Impl
{
    public class GripControllerImpl : GripController.GripControllerBase
    {
        private static readonly ILog _log = LogManager.GetLogger<RobotControllerImpl>();
        private readonly IRobotController _robotController;

        public GripControllerImpl(IRobotController robotController)
        {
            _robotController = robotController;
        }
        
        public override async Task<Grip_Responses> Grip(Grip_Parameters request, ServerCallContext context)
        {
            try
            {
                await SynchronizationController.Execute(() => { _robotController.Grip(true).Wait(); });
            }
            catch (Exception e)
            {
                _log.Warn(e);
                var error = ErrorHandling.CreateUndefinedExecutionError(e.Message);
                ErrorHandling.RaiseSiLAError(error);
            }
            return new Grip_Responses();
        }

        public override async Task<Release_Responses> Release(Release_Parameters request, ServerCallContext context)
        {
            try
            {
                await SynchronizationController.Execute(() => { _robotController.Grip(false).Wait(); });
            }
            catch (Exception e)
            {
                _log.Warn(e);
                var error = ErrorHandling.CreateUndefinedExecutionError(e.Message);
                ErrorHandling.RaiseSiLAError(error);
            }
            return new Release_Responses();
        }
    }
}