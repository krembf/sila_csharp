using System;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using Common.Logging;

namespace Biotek.Handler.Stacker
{
    internal class CommunicationController
    {
        private const int DEFAULT_TIMEOUT = 60000;
        private const int MAX_INPUT_BUFFER_SIZE = 2048;
        private const int MAX_OUTPUT_BUFFER = 2048;
        private const int WRITE_TIMEOUT_MS = 5000;
        private const int MAX_SERIAL_CONNECTION_ATTEMPTS = 3;
        private const int CONNECTION_ATTEMPTS_DELAY_MS = 1000;
        private const int ACK = 0x06;
        
        private static readonly ILog Log = LogManager.GetLogger<CommunicationController>();

        private readonly SerialPort serialPort = new SerialPort();

        internal CommunicationController(string comPort)
        {
            serialPort.WriteBufferSize = MAX_OUTPUT_BUFFER;
            serialPort.ReadBufferSize = MAX_INPUT_BUFFER_SIZE;
            serialPort.WriteTimeout = WRITE_TIMEOUT_MS;

            serialPort.PortName = comPort;
            serialPort.BaudRate = 9600;
            serialPort.Parity = Parity.None;
            serialPort.DataBits = 8;
            serialPort.ReadTimeout = DEFAULT_TIMEOUT;
            serialPort.StopBits = StopBits.Two;
            serialPort.Handshake = Handshake.None;
            serialPort.Encoding = Encoding.GetEncoding(28591); // 28591 is iso-8859-1 or Western European (ISO) encoding,
            serialPort.DtrEnable = true;
            serialPort.RtsEnable = true;
            // Make sure the data received event isn't set until the receive reply routine is hit
            serialPort.ReceivedBytesThreshold = 2500; // force the received event
        }
        
        public Message SendReceiveMessage(Command command)
        {
            var msg = new Message(command);
            return SendReceiveMessage(msg);
        }

        public Message SendReceiveMessage(Command command, char[] msgBody)
        {
            if (msgBody == null)
            {
                throw new ArgumentNullException(nameof(msgBody));
            }

            return SendReceiveMessage(new Message(command, msgBody));
        }


        /// <summary> SendMessage method
        /// Methods to send a serial message out the port
        /// </summary>
        private void SendMessage(Message message)
        {
            serialPort.DiscardOutBuffer();
            serialPort.DiscardInBuffer();

            Log.Debug($"\n{message}");
            WriteBuffer(message.Header.Data, Header.HEADER_SIZE);
            WriteBuffer(message.Body, message.Header.MessageBodySize);
        }

        /// <summary>
        /// Reads a response from the stacker 
        /// </summary>
        /// <returns></returns>
        /// <exception cref="InvalidOperationException"></exception>
        private Message ReceiveMessage()
        {
            var buffer = Enumerable.Repeat('\0', MAX_INPUT_BUFFER_SIZE).ToArray();
            const int offset = 0;
            SerialWaitForChars(1);

            serialPort.Read(buffer, offset, 1);
            if (Convert.ToByte(buffer[0]) != ACK)
            {
                throw new InvalidOperationException($"Failed to receive ACK, response was {buffer[0]}");
            }

            SerialWaitForChars(Header.HEADER_SIZE);
            var nBytesRead = serialPort.Read(buffer, offset, Header.HEADER_SIZE);

            if (nBytesRead != Header.HEADER_SIZE)
            {
                throw new InvalidOperationException($"Failed to receive complete Message Header");
            }

            var header = Header.Deserialize(buffer);
            var message = new Message(header);

            SerialWaitForChars(message.Header.MessageBodySize);
            nBytesRead = serialPort.Read(message.Body, offset, message.Header.MessageBodySize);

            if (nBytesRead != header.MessageBodySize)
            {
                throw new InvalidOperationException(
                    $"Failed to receive complete Message body. {nBytesRead} of {header.MessageBodySize} bytes read");
            }

            Log.Debug("Received message body");

            message.Validate();
            Log.Debug($"\n{message}");
            return message;
        }

        private void TryOpenSerialCom()
        {
            var nbTry = 0;
            while (!serialPort.IsOpen)
            {
                try
                {
                    serialPort.Open();
                    Log.Debug($"Successfully connected to serial port {serialPort.PortName}");
                }
                catch (UnauthorizedAccessException e)
                {
                    if (++nbTry > MAX_SERIAL_CONNECTION_ATTEMPTS)
                    {
                        Log.Error($"Last attempt to connect to serial port {serialPort.PortName} failed! Reason: {e.Message}");
                        throw;
                    }
                    Log.Debug($"Attempt to connect to serial port {serialPort.PortName} failed, retrying...");
                    Thread.Sleep(CONNECTION_ATTEMPTS_DELAY_MS);
                }
            }
        }
        
        private Message SendReceiveMessage(Message msg)
        {
            try
            {
                TryOpenSerialCom();
                SendMessage(msg);
                return ReceiveMessage();
            }
            finally
            {
                if (serialPort.IsOpen)
                {
                    serialPort.Close();
                }
            }
        }

        /// <summary> WriteBufferTimed method
        /// Methods to write data out the serial port
        /// </summary>
        /// <remarks>
        /// hardware handshaking off. We still look for CTS to be set on the instrument.
        /// See if the Clear-to-send line is set, to allow writing
        /// 10/5/2011 - CtsHolding was set to false when communicating with the PC via USB.
        /// Dtr and Rts were set to true on the serial port. We set these to true on the USB port setup also
        /// in the PC software (IS). This made the USB port work like the serial port and the following
        /// check passes.
        /// </remarks>
        private void WriteBuffer(char[] buffer, int numBytes)
        {
            if (!serialPort.CtsHolding)
            {
                throw new InvalidOperationException("The clear-to-send line is not set");
            }

            serialPort.Write(buffer, 0, numBytes);
        }

        /// <summary>
        /// Wait for the passed in number of chars to be available
        /// </summary>
        /// <param name="numChars"></param>
        /// <exception cref="TimeoutException"></exception>
        private void SerialWaitForChars(int numChars)
        {
            int bytesAvailable;
            var lStartTime = DateTime.Now.Ticks / 10000;

            do
            {
                bytesAvailable = serialPort.BytesToRead;

                Thread.Sleep(10);

                var totalTime = DateTime.Now.Ticks / 10000 - lStartTime;
                //TODO: are timout checks necessary here?
                if (totalTime > DEFAULT_TIMEOUT)
                {
                    throw new TimeoutException(
                        $"Failed to wait for {numChars} characters. Only {bytesAvailable} chars available");
                }
            } while (bytesAvailable < numChars);
        }
    }
}