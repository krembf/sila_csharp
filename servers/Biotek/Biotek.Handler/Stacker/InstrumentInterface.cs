namespace Biotek.Handler.Stacker
{
    public enum InstrumentInterface: byte
    {
        Primary = 0,
        Secondary = 1
    }
}
