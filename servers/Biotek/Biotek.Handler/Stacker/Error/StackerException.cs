using System;

namespace Biotek.Handler.Stacker
{
    [Serializable]
    public class StackerException : BiotekException
    {
        public StackerException(string message) : base(message)
        {
        }

        public StackerException(string message, Exception inner) : base(message, inner)
        {
        }
    }
}