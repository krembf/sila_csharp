namespace Biotek.Handler.Stacker
{
    public class StackerInstrumentException : StackerException
    {
        public BiostackError.Code Code { get; }
        
        public StackerInstrumentException(BiostackError.Code code, ushort errorCode) : base(code.ToString(errorCode))
        {
            this.Code = code;
        }
    }
}