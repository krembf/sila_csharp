namespace Biotek.Handler.Stacker
{
    public enum InstrumentType : ushort // todo should it be a signed short? 
    {
        None = unchecked((ushort)-1),
        PowerWaveGeneric = 0,
        SynergyGeneric = 1,
        ELx405 = 2,
        MicroFill = 3,
        Reserved = 4,
        PrecisionSeries = 5,
        CustomInstrument1 = 6,
        CustomInstrument2 = 7,
        CustomInstrument3 = 8,
        SynergyNeo = 9
    }
}