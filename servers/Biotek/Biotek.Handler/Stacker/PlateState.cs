namespace Biotek.Handler.Stacker
{
    public enum PlateState
    {
        FirstPlate,
        MiddlePlate,
        LastPlate,
        NoMorePlate
    }
}