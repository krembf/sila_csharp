using System;

namespace Biotek.Handler.Washer
{
    [Serializable]
    public class WasherException : BiotekException
    {
        public WasherException(string message) : base(message)
        {
        }
    }
}