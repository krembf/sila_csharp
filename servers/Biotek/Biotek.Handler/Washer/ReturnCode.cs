namespace Biotek.Handler.Washer
{
    public enum ReturnCode : short
    {
        Error = 0,
        Ok = 1,
        RegistrationFailure = 2,
        InterfaceFailure = 3,
        InvalidProductType = 4,
        OpenFileError = 5,
        PreRunError = 6
    }
}