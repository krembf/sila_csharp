namespace Biotek.Handler.Washer
{
    public enum RunStatus : short
    {
        Uninitialized = 0,
        Ready = 1,
        NotReady = 2,
        Busy = 3,
        Error = 4,
        Done = 5,
        Incomplete = 6,
        Paused = 7,
        StopRequested = 8,
        Stopping = 9,
        NotRequired = 10
    }
}