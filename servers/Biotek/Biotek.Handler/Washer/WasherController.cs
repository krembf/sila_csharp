using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Common.Logging;

namespace Biotek.Handler.Washer
{
    /// <summary>
    /// This is the implementation code for physical interaction with the instrument
    ///
    /// This is where we interact with the LHC API DLL etc..
    /// </summary>
    public class WasherController : IWasherController
    {
        private const int StatusCheckIntervalMs = 700;
        private static readonly ILog Logger = LogManager.GetLogger<WasherController>();
        private readonly IBtilhcRunner _btiHandler;
        private readonly string _productName;
        private readonly string _communicationPort;
        private readonly object _protocolRunLock = new object();
        private readonly List<string> _protocolsCache = new List<string>();
        private readonly object _protocolsCacheLock = new object();
        private string _previousProtocolRun = null;
        private Task _previousProtocolRunTask = null;
        private bool _isLhcRunnerInitialized = false;

        public WasherController(InstrumentType instrumentType, string communicationPort, string dllPath = "C:\\Program Files (x86)\\BioTek\\Liquid Handling Control 2.22\\BTILHCRunner.dll")
        {
            Logger.Info($"Loading dll at {dllPath}");
            var assembly = Assembly.LoadFrom(dllPath);
            this._btiHandler = new SynchronousBtilhcRunner(assembly.CreateInstance("BTILHCRunner.ClassLHCRunner").AlignToInterface<IBtilhcRunner>());
            this._productName = instrumentType.Value;
            this._communicationPort = communicationPort;
        }

        private void InitializeLhcRunner()
        {
            Logger.Info($"Setting the product name: {_productName}");
            // todo after this call the application won't stop because the LHCRunner internally create threads and does not interrupt them when the application ends
            HandleReturnCode((ReturnCode) _btiHandler.LHC_SetProductName(_productName));
            Logger.Info($"Set communication port: {_communicationPort}");
            HandleReturnCode(_btiHandler.LHC_SetCommunications(_communicationPort));
            Logger.Info($"Test communication");
            HandleReturnCode(_btiHandler.LHC_TestCommunications());
            HandleReturnCode(_btiHandler.LHC_SetRunnerThreading(1)); // Set LHC Runner to run in its own thread)
            _isLhcRunnerInitialized = true;
        }

        public void Initialize()
        {
            InitializeLhcRunner();
            var protocols = RetrieveProtocols();
            lock (_protocolsCacheLock)
            {
                _protocolsCache.Clear();
                _protocolsCache.AddRange(protocols);
            }
        }

        public void Dispose()
        {
        }

        public Task RunProtocol(string protocolName, CancellationToken cancellationToken)
        {
            if (!_isLhcRunnerInitialized)
            {
                throw new WasherException($"The washer is not initialized, please make sure to initialize it");
            }
            lock (_protocolRunLock)
            {
                if (_previousProtocolRunTask != null &&
                    !(_previousProtocolRunTask.IsCanceled || _previousProtocolRunTask.IsCompleted ||
                      _previousProtocolRunTask.IsFaulted))
                {
                    throw new WasherException("Another protocol is already running!");
                }
                
                _previousProtocolRunTask = Task.Run(async () =>
                {
                    cancellationToken.Register(() =>
                    {
                        Logger.Info($"Aborting Run Protocol {protocolName} task");
                        HandleReturnCode(_btiHandler.LHC_PauseProtocol());
                        HandleReturnCode(_btiHandler.LHC_AbortProtocol());
                        Logger.Info($"Aborted Run Protocol {protocolName} task");
                    });
                    if (cancellationToken.IsCancellationRequested)
                    {
                        Logger.Info($"Run Protocol {protocolName} task cancelled before starting it");
                        return;
                    }

                    if (_previousProtocolRun != null && _previousProtocolRun == protocolName)
                    {
                        Logger.Info($"Bypassing validation of protocol `{protocolName}` because it was already validated once");
                        HandleReturnCode(_btiHandler.LHC_OverrideValidation(1)); // Bypass validation
                    }
                    else
                    {
                        Logger.Info($"Loading protocol: {protocolName}");
                        HandleReturnCode(_btiHandler.LHC_LoadProtocolFromFlash(protocolName));
                        Logger.Info($"Validating protocol when running `{protocolName}`");
                        HandleReturnCode(_btiHandler.LHC_OverrideValidation(0)); // validate protocol
                    }
                    _previousProtocolRun = protocolName;
                    Logger.Info("Testing communication");
                    HandleReturnCode(_btiHandler.LHC_TestCommunications());
                    Logger.Info("Run protocol");
                    cancellationToken.ThrowIfCancellationRequested();
                    var runStatus = (RunStatus) _btiHandler.LHC_RunProtocol();
                    Logger.Info($"Run Status: {runStatus}");
                    Logger.Info("Starting polling for completion");
                    while (runStatus != RunStatus.Done && runStatus != RunStatus.NotReady && runStatus != RunStatus.Ready)
                    {
                        if (runStatus == RunStatus.Error)
                        {
                            HandleInternalError();
                        }

                        await Task.Delay(StatusCheckIntervalMs);
                        var prevStatus = runStatus;
                        runStatus = (RunStatus) _btiHandler.LHC_GetProtocolStatus();
                        if (prevStatus != runStatus)
                        {
                            Logger.Info($"Run Status: {runStatus}");
                        }
                    }
                    cancellationToken.ThrowIfCancellationRequested();
                    Logger.Info("Run completed");
                }, cancellationToken);
                return _previousProtocolRunTask;
            }
        }

        public IEnumerable<string> GetProtocols()
        {
            if (!_isLhcRunnerInitialized)
            {
                throw new WasherException($"The washer is not initialized, please make sure to initialize it");
            }
            lock (_protocolsCacheLock)
            {
                return new List<string>(_protocolsCache);
            }
        }

        private IEnumerable<string> RetrieveProtocols()
        {
            if (!_isLhcRunnerInitialized)
            {
                throw new WasherException($"The washer is not initialized, please make sure to initialize it");
            }
            lock (_protocolRunLock)
            {
                if (_previousProtocolRunTask != null && !(_previousProtocolRunTask.IsCanceled || _previousProtocolRunTask.IsCompleted ||
                      _previousProtocolRunTask.IsFaulted))
                {
                    _previousProtocolRunTask.Wait();
                }

                short numProtocols = 0;
                Logger.Info("Get on-board protocol count");
                var isReturnCode = _btiHandler.LHC_GetOnBoardProtocolCount(ref numProtocols);
                if (isReturnCode != 1)
                {
                    HandleInternalError();
                    return new List<string>();
                }

                Logger.Info($"Number of protocols found: {numProtocols}");
                var protocols = new List<string>();
                for (short i = 0; i < numProtocols; i++)
                {
                    protocols.Add(_btiHandler.LHC_GetOnBoardProtocolName(i));
                }

                Logger.Info(string.Join(",", protocols));
                return protocols;
            }
        }

        /// <summary>
        /// Checks if return value is non 1 i.e. is an error, gets the error and creates an exception
        ///
        /// TODO: better error handling with modelled errors according to documentation
        ///  
        /// </summary>
        /// <param name="returnCode">the return value from an LCH handler execution</param>
        /// <exception cref="Exception">An LHC error, TODO: model different error types if needed</exception>
        private void HandleReturnCode(ReturnCode returnCode)
        {
            if (returnCode == ReturnCode.Ok)
            {
                return;
            }
            //TODO: Maybe error has to first be retrieved for
            if (returnCode == ReturnCode.InterfaceFailure)
            {
                HandleInternalError();
            }

            var errorString = _btiHandler.LHC_GetErrorString((short) returnCode);
            throw new WasherException($"{errorString}, code {returnCode}");
        }
        
        private void HandleReturnCode(short returnCode)
        {
            HandleReturnCode((ReturnCode)returnCode);
        }

        /// <summary>
        /// Retrieves last IS error and throws an exception
        /// </summary>
        private void HandleInternalError()
        {
            var errorCode = _btiHandler.LHC_GetLastErrorCode();
            var errorString = _btiHandler.LHC_GetErrorString(errorCode);
            throw new WasherInternalException($"{errorString}, code {errorCode}");
        }
    }
}