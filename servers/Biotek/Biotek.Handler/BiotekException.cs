using System;
using System.Runtime.Serialization;

namespace Biotek.Handler
{
    [Serializable]
    public class BiotekException : Exception
    {
        public BiotekException()
        {
        }

        public BiotekException(string message) : base(message)
        {
        }

        public BiotekException(string message, Exception inner) : base(message, inner)
        {
        }

        protected BiotekException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}